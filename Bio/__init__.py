# -*- coding: utf-8 -*-
from .bio import Bio

async def setup(bot):
    await bot.add_cog(Bio(bot))