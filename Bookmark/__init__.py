# -*- coding: utf-8 -*-
from .bookmark import Bookmark

async def setup(bot):
    await bot.add_cog(Bookmark(bot))