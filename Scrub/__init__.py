# -*- coding: utf-8 -*-
from .scrub import Scrub

async def setup(bot):
    await bot.add_cog(Scrub(bot))